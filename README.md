<img src="scout.png" width="50">

### Scout

Easily schedule, automate and manage tasks.

``` javascript
let scout = require("scout");

scout("Monday thru Thursday at 9:30am", () => {
    scout.exec("brew update && brew upgrade && brew cleanup", () => {
        scout.notify("Brew and your installed formulas have been updated")
    });
});
```

### Cli flags

##### `scout run`

Used primarily from the cron job to execute the contents of `~/.scout.js`.

##### `scout open`

Opens your `~/.scout.js` file in your default editor.

##### `scout init`

Creates `~/.scout.js` if it doesn't already exist.

##### `scout stop`

Removes the cron job that runs Scout from the crontab.

##### `scout start`

Adds the cron job to the crontab to run Scout every minute.

##### `scout status`

Quick check to see if Scout is set in the crontab.

##### `scout --completion`

Outputs contents for a `bash-completion` file.

### Usage

##### `scout("query", callback())`

Calling Scout as a function determines if the query matches your current time/date based on how it's written. If it passes successfully, the callback is then called upon.

``` javascript
scout("every Monday in June and July at 9:30am", () => {
    // Do something
});
```

##### `scout.exec("command", callback())`

Synchronously executes a shell command. If a callback is supplied, it'll be called and passed the output from the command. Otherwise the output of the command is returned.

``` javascript
scout.exec("whoami", me => {
    console.log(`The user is ${me}`);
});

// or

let me = scout.exec("whoami");
console.log(`The user is ${me}`);
```

##### `scout.notify([message])`

Sends out a nativate notification on your machine with the main tile and icon autofilled.

``` javascript
scout.notify("🎉 Something awesome just happened");
```

##### `scout.download(url|[urls], destination)`

Helper function to download one or multiple file URLs to the specified destination. If no destination is supplied, `~/Downloads` will be defaulted to. The destination path is automatically expanded, meaning there's no need to supply a full absolute-path.

``` javascript
// Download a single file
scout.download("https://images.unsplash.com/photo-1504006833117-8886a355efbf", "~/Pictures");

// Download multiple files
scout.download([
    "https://images.unsplash.com/photo-1504006833117-8886a355efbf",
    "https://images.unsplash.com/photo-1504125130065-19cd3d71c27a"
], "~/Pictures");
```
