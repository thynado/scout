let Conf = require("conf");
let path = require("path");
let execa = require("execa");
let moment = require("moment");
let untildify = require("untildify");
let notifier = require("node-notifier");
let config = new Conf();

/**
 * Parse month mentions
 * 
 * @return boolean
 */
function getMonths (input) {
    let months = moment.months().concat(moment.monthsShort()).join("|");
    let ranges = input.match( new RegExp(`(${months}) (?:thru|to|through|-) (${months})`) );
    let withinAnd = input.match( new RegExp(`(?:every |in )?(${months}) (?:and|or) (${months})`) )
    let within = input.match( new RegExp(`(?:every |in )?(${months})`) )

    if (ranges) {
        let start = moment(ranges[1], "MMM");
        let end = moment(ranges[2], "MMM");
        let now = moment();

        return (now.month() >= start.month()) && (now.month() <= end.month());
    } else if (withinAnd) {
        let start = moment(withinAnd[1], "MMM");
        let end = moment(withinAnd[2], "MMM");
        let now = moment();
        
        return [start.month(), end.month()].includes(now.month());
    } else if (within) {
        let target = moment(within[1], "MMM");
        let now = moment();
        
        return now.month() === target.month();
    }
}

/**
 * Parse day mentions
 * 
 * @return boolean
 */
function getDays (input) {
    let days = moment.weekdays().concat(moment.weekdaysShort(), moment.weekdaysMin()).join("|");
    let range = input.match( new RegExp(`(${days}) (thru|to|through|-|and|or) (${days})`) );
    let every = input.match( new RegExp(`(?:every |on )?(${days})`) )

    if (range) {
        let start = moment(range[1], "dddd");
        let operand = range[2];
        let end = moment(range[3], "dddd");
        let now = moment();

        if (["and", "or"].includes(operand)) {
            return (now.weekday() == start.weekday()) || (now.weekday() == end.weekday());
        }

        return (now.weekday() >= start.weekday()) && (now.weekday() <= end.weekday());
    } else if (every) {
        return moment().weekday() === moment(every[1], "dddd").weekday();
    }
}

/**
 * Parse time mentions
 * 
 * @return boolean
 */
function getTimes (input) {
    let expr = "\\b((?:(?:[0-1][0-2]|[0-9]):[0-5][0-9]|[0-1]?[0-9]) ?[APap][mM])";
    let times = input.match(new RegExp(expr, "g"));
    let point = input.match(new RegExp(`at ${expr}`));
    let after = input.match(new RegExp(`after ${expr}`));
    let before = input.match(new RegExp(`before ${expr}`));
    let range = input.match(new RegExp(`${expr} (thru|to|through|-) ${expr}`));

    if (point) {
        let then = moment(times[0], "HH:mm a").format("HH:mm a");
        let now = moment().format("HH:mm a");

        return then === now;
    } else if (range) {
        let start = moment(times[0], "HH:mm a");
        let end = moment(times[1], "HH:mm a");
        let now = moment();

        return now.isBefore(end) && now.isAfter(start)
    } else if (after) {
        let then = moment(times[0], "HH:mm a");
        let now = moment();

        return now >= then;
    } else if (before) {
        let then = moment(times[0], "HH:mm a");
        let now = moment();

        return now < then;
    }
}

/**
 * Single run processor for hours, 
 * days, months and years.
 * 
 * @return boolean
 */
function perCatch (type, input) {
    let passed = true;
    let now = moment();
    let last = config.get(`per.${type}`, {});

    if (last[input]) {
        passed = (now[type]() != moment(last[input])[type]());
    }

    if (passed) {
        last[input] = moment().toISOString();
        config.set(`per.${type}`, last);
    }

    return passed;
}

/**
 * Single run patterns.
 * 
 * @return boolean
 */
function per (input) {
    let passed = true;
    let checks = [
        [input.match(/once (per|a|an) hour/), "hour"],
        [input.match(/once (per|a) day/), "day"],
        [input.match(/once (per|a) month/), "month"],
        [input.match(/once (per|a) year/), "year"]
    ]

    checks.some(check => {
        if (check[0]) {
            passed = perCatch(check[1], input);
            return true;
        }
    });

    return passed;
}

/**
 * Does the input pass the month, day and time filters...
 * 
 * @return boolean
 */
function isValid (input) {
    let passed = true;
    let handles = [getDays, getMonths, getTimes];

    for (let index in handles) {
        if (!passed) {
            break
        }

        let response = handles[index].call(this, input);
        if (response == false || response == true) {
            passed = response;
        }
    }
    
    if (passed) {
        return per(input);
    }

    return passed;
}

/**
 * Default caller.
 * 
 * @param String - input - User-friendly string passed to validation
 * @param Function - handle - Optional callback
 * @return mixed
 */
let scout = (input, handle) => {
    if (handle) {
        isValid(input) && handle();
    } else {
        return {
            except (input, handle) {
                !isValid(input) && handle();
            }
        }
    }
}

/**
 * Send a notification
 * 
 * @param String - message - Message of the notification
 * @return void
 */
scout.notify = message => {
    notifier.notify({
        title: "Scout",
        message: message,
        icon: path.join(__dirname, 'scout.png'),
        sound: "Funk"
    });
}

/**
 * Provides MomentJS.
 * 
 * @type Object
 */
scout.moment = moment;

/**
 * Execute a command synchronously.
 * 
 * @param String - command - Command ran in shell
 * @param Function - handle - Optional callback with stdout
 * @return string or callback
 */
scout.exec = (command, handle) => {
    let response = execa.shellSync(command);

    return handle ? handle(response.stdout) : response.stdout;
}

/**
 * Download files to a destination.
 * 
 * @param String|Array - url - One or more URLs to download
 * @param String - dest - The destination path for the download, default is ~/Downloads
 * @return void
 */
scout.download = (url, dest) => {
    let downloads = {};
    let urls = url.splice ? url : [url];

    dest = dest ? dest.replace(/\/$/, '') + "/" : "~/Downloads/";

    urls.forEach(url => {
        let destination = `${untildify(dest)}${path.basename(url)}`;
        
        downloads[url] = destination;
        execa.shellSync(`curl "${url}" > "${destination}"`);
    });

    return downloads;
}

module.exports = scout;